module.exports = {
  theme: {
    boxShadow: {
      'new': '0px 0px 4px 2px rgba(43,108,176,0.88), 0px 0px 4px 2px rgba(43,108,176,0.88)'
    },
    extend: {},
    fontFamily: {
        'display':["Open Sans", "Roboto"]
      },
  },
  variants: {
    shadowColor:['responsive', 'hover', 'focus']
  },
  plugins: [],
};
